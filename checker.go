package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"

	"text/template"
)

type Config struct {
	Steps []*CheckStep `yaml:"steps"`
}

type CheckStep struct {
	PackageURL string   `yaml:"url"`
	Glob       bool     `yaml:"glob"`
	Commands   []string `yaml:"commands"`
}

func (c *CheckStep) Script() string {
	globpath := ""
	if c.Glob {
		globpath = "/..."
	}

	commands := []string{
		"set -e",
		fmt.Sprintf("go get -d -u -v %s%s", c.PackageURL, globpath),
	}
	for _, cmd := range c.Commands {
		commands = append(commands, fmt.Sprintf("%s %s%s", cmd, c.PackageURL, globpath))
	}
	return strings.Join(commands, "\n")
}

type Report struct {
	PackageResults []*Result `json:"results"`
}

func withWriter(filepath string, writeFunc func(io.Writer) error) error {
	var writer io.Writer
	if filepath != "" {
		f, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0755)
		if err != nil {
			return fmt.Errorf("failed to create file: %s", err)
		}

		defer f.Close()
		writer = f
	} else {
		writer = os.Stdout
	}

	return writeFunc(writer)
}

func (r *Report) Write(filepath string) error {
	err := withWriter(filepath, func(writer io.Writer) error {
		return json.NewEncoder(writer).Encode(r)
	})
	if err != nil {
		return fmt.Errorf("failed to encode json result: %s", err)
	}

	return nil
}

func (r *Report) WriteHTML(templatePath, filepath string) error {
	tmpl, err := template.ParseFiles(templatePath)
	if err != nil {
		return fmt.Errorf("failed to parse dashboard template: %s", err)
	}

	err = withWriter(filepath, func(writer io.Writer) error {
		return tmpl.Execute(writer, r)
	})

	if err != nil {
		return fmt.Errorf("failed to execute template: %s", err)
	}

	return nil
}

type Result struct {
	Package string `json:"package"` // package url
	Success bool   `json:"success"` // indicates that step was successful
	Error   string `json:"error"`   // error that caused step interruption
	StdOut  string `json:"stdout"`  // step's stdout
	StdErr  string `json:"stderr"`  // step's stderr
}

type Checker struct {
	scriptRunner ScriptRunner
}

func NewChecker() *Checker {
	return &Checker{&CmdScriptRunner{}}
}

func (c *Checker) Check(config *Config) *Report {
	report := &Report{[]*Result{}}

	for _, step := range config.Steps {
		report.PackageResults = append(report.PackageResults, c.PerformStep(step))
	}

	return report
}

func (c *Checker) PerformStep(step *CheckStep) *Result {
	result := &Result{Package: step.PackageURL, Success: false}
	stdout, stderr, err := c.scriptRunner.Run(step.Script())
	if err != nil {
		result.Error = fmt.Sprintf("Falied to run script: %s", err)
		return result
	}

	result.Success = true
	result.StdOut = stdout
	result.StdErr = stderr
	return result
}

type ScriptRunner interface {
	Run(script string) (string, string, error)
}

type CmdScriptRunner struct {
}

func (sr *CmdScriptRunner) Run(script string) (string, string, error) {
	log.Printf("Running step script:\n%s\n\n", script)
	cmd := exec.Command("sh")
	cmd.Stdin = strings.NewReader(script)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "", "", fmt.Errorf("failed to pipe to stdout: %s", err)
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return "", "", fmt.Errorf("failed to pipe to stderr: %s", err)
	}

	if err := cmd.Start(); err != nil {
		return "", "", fmt.Errorf("process failed to start: %s", err)

	}

	stdoutBuf := bytes.NewBuffer(nil)
	stderrBuf := bytes.NewBuffer(nil)
	go func() {
		if _, err := io.Copy(stdoutBuf, io.TeeReader(stdout, os.Stdout)); err != nil {
			log.Printf("Failed to capture stdout: %s\n", err)
		}

		if _, err := io.Copy(stderrBuf, io.TeeReader(stderr, os.Stderr)); err != nil {
			log.Printf("Failed to capture stderr: %s\n", err)
		}
	}()

	if err := cmd.Wait(); err != nil {
		return "", "", fmt.Errorf("step process failed: %s", err)
	}

	return stdoutBuf.String(), stderrBuf.String(), nil
}
