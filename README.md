# ci-dashboard

Utility to bulk test golang packages.

ci-dashboard will `go get` package, execute requested commands and
will return test report. Configuration is provided in yaml
format. Report by default uses json format, html templates are also
supported.

## Installation

ci-dashboard is a conventional golang utility, regular "go get" will
work on this repository:

```bash
$ go get -v ./...
```

## Usage

ci-dashboard doesn't have mandatory arguments. By default it will try
to load configuration from the `config.yml` file in current
folder. General usage format:

```bash
$ ci-dashboard [flags] [config.yml]
```

Optional configuration flags:

* `-html` - html template location. If provided report will be generated using this template.
* `-out`  - Location of a report file. If not provided report will be sent to stdout.

## YAML configuration format

```yaml
steps:
  - url: github.com/skycoin/skycoin
    glob: true
    commands:
      - go test
      - go build
```

With `glob` argument ci-dashboard will test all sub-packages by appending `./...`.

## Report's JSON schema

```golang
type Report struct {
	PackageResults []*Result `json:"results"` // individual results for each package
}

type Result struct {
	Package    string `json:"package"`    // package url
	Success    bool   `json:"success"`    // indicates that step was successful
	Error      string `json:"error"`      // error caused step interruption
	StdOut     string `json:"stdout"`     // step's stdout
	StdErr     string `json:"stderr"`     // step's stderr
}
```

Sample report
```json
{
  "results": [
    {
      "package": "gitlab.com/skycoin-dev/skycoin-node-sync-test",
      "success": true,
      "error": "",
      "stdout": "ok   gitlab.com/skycoin-dev/skycoin-node-sync-test (cached)\n",
      "stderr": "Fetching https://gitlab.com/skycoin-dev/skycoin-node-sync-test?go-get=1\nParsing meta tags from https://gitlab.com/skycoin-dev/skycoin-node-sync-test?go-get=1 (status code 200)\nget \"gitlab.com/skycoin-dev/skycoin-node-sync-test\": found meta tag get.metaImport{Prefix:\"gitlab.com/skycoin-dev/skycoin-node-sync-test\", VCS:\"git\", RepoRoot:\"https://gitlab.com/skycoin-dev/skycoin-node-sync-test.git\"} at https://gitlab.com/skycoin-dev/skycoin-node-sync-test?go-get=1\ngitlab.com/skycoin-dev/skycoin-node-sync-test (download)\ngithub.com/skycoin/skycoin (download)\n"
    }
  ]
}
```

## HTML reports

You can provide custom template that uses golang's `template` package
syntax, this template will be executed with `Report` in scope.

Sample template:

```html
<!DOCTYPE html>
<html>
  <body>
    <ul>
      {{range .RepoResults}}
      <li>
        <h1>{{.Package}}</h1>
        <p>
          {{if .Success}}
          <span>Success</span>
          {{else}}
          <span>Failure: </span>
          {{.Error}}
          {{end}}
        </p>
        <p>StdOut: <code>{{.StdOut}}</code></p>
        <p>StdErr: <code>{{.StdErr}}</code></p>
      </li>
      {{ end }}
    </ul>
  </body>
</html>
```
